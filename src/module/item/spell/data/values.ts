export const MAGIC_SCHOOLS = [
    "abjuration",
    "conjuration",
    "divination",
    "enchantment",
    "evocation",
    "illusion",
    "necromancy",
    "transmutation",
] as const;

export const MAGIC_TRADITIONS = ["arcane", "divine", "occult", "primal"] as const;
